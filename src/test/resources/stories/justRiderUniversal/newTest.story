Feature: testing params

Meta:
@onPeakTime 9PM
@offPeakTime 6PM

Scenario: An adult rider scans a single use peak ticket during peak hours and the ticket is accepted and killed
Given A rider has a single ticket with no entitlements
And Only the peak fare block
When The ticket is scanned during peak time
Then The ticket is accepted
And Using default params determined based on peak time