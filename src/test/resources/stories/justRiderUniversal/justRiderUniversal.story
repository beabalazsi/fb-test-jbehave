Feature: dasdasdaad


Scenario: An adult rider scans a single use peak ticket during peak hours and the ticket is accepted and killed
Given A rider has a single ticket with no entitlements
And Only the peak fare block
When The ticket is scanned during peak time
Then The ticket is accepted
And Killed with 5001

Scenario: An adult rider scans a single use peak ticket during off-peak hours and the ticket is accepted and killed
Given A rider has a single ticket with no entitlements
And Only the peak fare block
When The ticket is scanned during off-peak time
Then The ticket is accepted
And Killed with 5001

Scenario: An adult rider scans a single use off-peak ticket during peak hours and the ticket is rejected
Given A rider has a single ticket with no entitlements
And Only the off-peak fare block
When The ticket is scanned during peak time
Then The ticket is rejected with 2025

Scenario: An adult rider scans a single use off-peak ticket during off-peak hours and the ticket is accepted and killed
Given A rider has a single ticket with no entitlements
And Only the off-peak fare block
When The ticket is scanned during off-peak time
Then The ticket is accepted
And Killed with 5001

Scenario: An elderly rider scans a single use peak ticket during peak hours and the ticket is accepted with a warning to check ID
Given A rider has a ticket with the elderly entitlement
And Only the peak fare block
When The ticket is scanned during peak time
Then The ticket is accepted with a warning 3106

Scenario: An elderly rider scans a single use off-peak ticket during peak hours and the ticket is accepted rejected
Given A rider has a ticket with the elderly entitlement
And Only the off-peak fare block
When The ticket is scanned during peak time
Then The ticket is rejected with 2025

Scenario: A low income rider scans a single use peak ticket during off-peak hours and the ticket is accepted and killed
Given A rider has a single ticket with the low income entitlements
And Only the peak fare block
When The ticket is scanned during off-peak time
Then The ticket is accepted and killed with 5001

Scenario: A low income rider scans a single use off-peak ticket during peak hours and the ticket is rejected
Given A rider has a single ticket with the low income entitlements
And Only the off-peak fare block
When The ticket is scanned during peak time
Then The ticket is rejected with 2025

Scenario: A ticket with the incorrect fareblocks is scanned on a validator. It is rejected and the validator displays the string specified in the validation rules
Given A ticket has fareblock A
And A validator has fareblock B.
And Incorrect fareblocks are configured to result in rejection
When The ticket is scanned on the validator
Then The ticket is rejected with 2025
And the string from the validation rules is displayed on the screen

Scenario: A ticket with the incorrect fareblocks is scanned on a validator. It is flagged up for manual inspection and the validator displays the string specified in the validation rules
Given A ticket has fareblock A
And A validator has fareblock B
And Incorrect fareblocks are configured to result in a warning
When The ticket is scanned on the validator
Then The ticket is flagged for manual inspection with 3100
And the string from the validation rules is displayed on the screen

Scenario: A ticket with the correct fareblock and no entitlements is scanned on a validator. It is accepted and the validator displays the string specified in the validation rules
Given A ticket (no entitlements)
And A validator share a common fareblock
When The ticket is scanned on the validator
Then The ticket is accepted with 4017 or 5001
And the string from the validation rules is displayed on the screen

Scenario: A ticket with the off-peak fareblock is scanned on a validator during peak time. The phone time is set to off-peak time. The ticket is rejected by the validator
Given A rider has an off-peak ticket
And Has set their phone time to the off-peak time
And It is actually peak time
When The ticket is scanned on the validator
Then The ticket is rejected with 2027

Scenario: A ticket with a valid fareblock is tapped against a validator that has an AVL fareblock enabled. The ticket is accepted by the validator
Given A rider has a ticket that is only valid at station A.
And An onboard validator is located on a vehicle that has just pulled into station A
When The ticket is scanned on the validator
Then The ticket is accepted with 4017 or 5001.
!--  Note that this is not yet implemented but we will need to be able to test this. We should be able to spoof the GPS coordicates of a validator or otherwise make this fareblock active in a way that allows meaningful testing

Scenario: A ticket with an invalid fareblock is tapped against a validator that has an AVL fareblock enabled. The ticket is rejected by the validator
Given A rider has a ticket that is not valid at station A
And An onboard validator is located on a vehicle that has just pulled into station A
When The ticket is scanned on the validator
Then The ticket is rejected with 2025.
!--  Note that this is not yet implemented but we will need to be able to test this. We should be able to spoof the GPS coordicates of a validator or otherwise make this fareblock active in a way that allows meaningful testing

Scenario: A token with healthy balance and no products is tapped on a validator. The validator has no configured fareblocks. The token is accepted, provisioned with an appropriate product and charged accordingly
Given A token has a healthy balance
And Has no products
And A validator has no configured fareblocks
When The token is tapped on the validator
Then The token is accepted with 4017
And Provisioned with the cheapest available product
And Charged for that product

Scenario: A token with healthy balance and has already been provisioned a single use product. The price difference between the single use product and a day pass is less than the cost of the single use product. A validator has no configured fareblocks. When tapped, the token is accepted, provisioned with a day pass and charged accordingly
Given A token has a healthy balance
And Has been provisioned with a single use product that day.
And The price difference between the single use product and a day pass is less than the cost of another single use product
And A validator has no active fareblocks.
When The token is tapped on the validator
Then The token is accepted with 4017
And Provisioned with a day pass
And Charged for that product

Scenario: A token with healthy balance and and a day pass is tapped on a validator. The validator has no configured fareblocks. The token is accepted but not provisioned with any products or charged
Given A token has a healthy balance
And Has a day pass
And A validator has no configured fareblocks
When The token is tapped on the validator
Then The token is accepted with 4017
And No further action is taken


Scenario: A token with low funds and no products is tapped on a validator. The validator has no configured fareblocks. The token is accepted, provisioned with the product and charged accordingly
Given A token has low funds
And Has no products
And A validator has no configured fareblocks
When The token is tapped on the validator
Then The token is accepted with recommended action of 3000 and a warning displayed. The actual action should be 4017


Scenario: A token with low funds and no products is tapped on a validator. The validator has no configured fareblocks. The token is accepted and no further action is taken
Given A token has low funds
And Has a day pass
And A validator has no configured fareblocks
When The token is tapped on the validator
Then The token is accepted with 4017
And No further action is taken.
!-- Note that this behaviour might not be seen currently but is what customers want


Scenario: A token with insufficient funds and no products is scanned on a validator. The validator has no configured fareblocks. The token is rejected and no further action is taken
Given A token has insufficient funds
And Has no provisioned products
And A validator has no active fareblocks
When The token is tapped on the validator
Then The token is rejected with 2032
And No further action is taken


Scenario: A token with insufficient funds and a day pass is scanned on a validator. The validator has no configured fareblocks. The token is accepted and no further action is taken
Given A token has insufficient funds
And Has a day pass
And A validator has no active fareblocks
When The token is tapped on the validator
Then The token is accepted with 4017.
And No further action is taken


Scenario: A token with insufficient funds and a day pass is scanned on a validator. The validator has no configured fareblocks. The token is accepted and no further action is taken
Given A token has insufficient funds
And Has a day pass
And A validator has no active fareblocks
When The token is tapped on the validator
Then The token is accepted with 4017.
And No further action is taken
