package utils;

import com.masabi.justride.broker.rest.json.v3.entity.ProductDTO;
import io.restassured.RestAssured;
import org.apache.http.HttpStatus;

import static io.restassured.RestAssured.when;

public class BrokerAdapter {

    public BrokerAdapter() {
        RestAssured.baseURI = "https://uw2-uat-pay-paympg.masabi-sandbox.systems";
    }

    public ProductDTO getAbtProductLookup(BrandID brandID,String selectionKey,String productRestrictionName,String fulFilmentType) {

        return when()
                .get("/api/v1/{brandId}/products/lookup?selectionkey={selectionKey}&productrestrictionname={productRestrictionName}&time={timestamp}&fulfilmenttype={fulfilmentType}"
                ,brandID.name(),selectionKey,productRestrictionName,System.currentTimeMillis(),fulFilmentType)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(ProductDTO.class);
    }
}
