package utils;

import java.util.Map;

public class Environments {
    private static final Map<String, Environment> environments = Map.of(
            Environment.INTEGRATION.name(), Environment.INTEGRATION,
            Environment.UAT.name(), Environment.UAT,
            Environment.PROD_US.name(), Environment.PROD_US,
            Environment.PROD_EU.name(), Environment.PROD_EU);

    public static String getBaseUrl() {
        // set to integration as default
        if (System.getProperty("environment") == null || environments.get(System.getProperty("environment")) == null) {
            // TODO add logging and lombok
            return Environment.INTEGRATION.baseUrl;
        }
        return environments.get(System.getProperty("environment")).baseUrl;
    }

    private enum Environment {
        INTEGRATION("integration", "https://integration.masabi-sandbox.systems/"),
        UAT("uat", "https://uat.justride.systems/"),
        PROD_US("prod_us", ""),
        PROD_EU("prod_eu", "");
        private final String envName;
        private final String baseUrl;

        Environment(String envName, String baseUrl) {
            this.envName = envName;
            this.baseUrl = baseUrl;
        }
    }
}
