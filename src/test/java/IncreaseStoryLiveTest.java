import org.jbehave.core.Embeddable;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.reporters.SurefireReporter;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import steps.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

public class IncreaseStoryLiveTest extends JUnitStories {

    @Override
    public Configuration configuration() {
        Properties viewResources = new Properties();
        viewResources.setProperty("decorateNonHtml", "false");
        Class<? extends Embeddable> embeddableClass = this.getClass();
        SurefireReporter surefireReporter = new SurefireReporter(embeddableClass);
        return new MostUsefulConfiguration()
                .useStoryLoader(new LoadFromClasspath(this.getClass()))
                .useStoryReporterBuilder(new StoryReporterBuilder()
                        .withSurefireReporter(surefireReporter)
                        .withCodeLocation(codeLocationFromClass(this.getClass()))
                        .withViewResources(viewResources)
                        .withFormats(Format.CONSOLE, Format.HTML, Format.HTML_TEMPLATE));
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new IncreaseSteps(), new ActionSteps(), new PrerequisiteSteps(), new ValidationSteps(), new TicketPrerequisiteSteps());
    }

    @Override
    protected List<String> storyPaths() {
        String brand = System.getProperty("brand") != null ? System.getProperty("brand") : "justRiderUniversal";

        String location = String.format("stories/%s", brand);

        if (System.getProperty("brand") == null) {
            File[] files = new File("src/test/resources/stories").listFiles();
            List<String> listOfStoryFiles = new ArrayList<>();
            Arrays.stream(files).forEach(file -> {
                if (file.isFile()) {
                    listOfStoryFiles.add("stories" + file.getName());
                } else {
                    listOfStoryFiles.addAll(Arrays.stream(file.getAbsoluteFile().listFiles())
                            .filter(File::isFile)
                            .map(fileFromSub -> "stories" + fileFromSub.getName())
                            .collect(Collectors.toList()));
                }
            });
            return listOfStoryFiles;
        } else {
            File[] files = new File("src/test/resources/" + location).listFiles();
            return Arrays.stream(files)
                    .filter(File::isFile)
                    .map(file -> location + "/" + file.getName())
                    .collect(Collectors.toList());
        }
    }

}