package steps;

import org.assertj.core.api.SoftAssertions;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;

public class ValidationSteps {

    @Then("The ticket is valid")
    public void validateTicket() {
        System.out.println("Ticket is valid");
    }


    @Then("The token is $isAcceptedOrRejected with $code")
    public void validateTokenIsAcceptedWithCode(String isAcceptedOrRejected, String code) {
        System.out.println(String.format("Token is %s with code %s", isAcceptedOrRejected, code));
    }


    @Then("The ticket is accepted with $code")
    public void validateTicketIsAcceptedWithCode(String code) {
        System.out.println("Token is accepted with code " + code);
    }

    @Then("The ticket is rejected with $code")
    public void validateTicketIsRejectedWithCode(String code) {
        System.out.println("Token is rejected with code " + code);

    }

    @Then("No further action is taken")
    public void noFurtherActionIsTaken() {
        System.out.println(" No more Further action is taken");
    }

    @Then("the string from the validation rules is displayed on the screen")
    public void theValidationStringIsDisplayedONScreen() {
        System.out.println("theValidationStringIsDisplayedONScreen");
    }

    @Then("The ticket is accepted")
    public void theTicketIsAccepted() {
        System.out.println("theTicketIsAccepted");
        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat("one ".equals("two"));
        softAssertions.assertThat(1 == 2);
        softAssertions.assertAll();

    }

    @Then("Killed with $code")
    public void ticketOrTokenKilledWithCode(String code) {
        System.out.println(String.format("ticketOrTokenKilledWithCode %s", code));
    }

    @Then("Using default params determined based $peakTime")
    public void ticketOrTokenKilledWithCodeWithMEtaParams(String isPeak,@Named("onPeakTime") String onPeakTime, @Named("offPeakTime") String offPeakTime) {
        if (isPeak.equalsIgnoreCase("on peak time")) {
            System.out.println("Using time: "+ onPeakTime);
        } else {
            System.out.println("Using time: "+ offPeakTime);
        }
    }

    @Then("Charged for that product")
    public void validateRiderIsChargedForProduct() {
        System.out.println("validateRiderIsChargedForProduct");
    }

    @Then("Provisioned with $provisionedProduct")
    public void validateRiderIsProvisionedWithProduct(String provisionedProduct) {
        System.out.println(String.format("validateRiderIsProvisionedWithProduct %s", provisionedProduct));
    }

    @Then("The ticket is flagged for $inspectionType with $code")
    public void validateTicketIsFlaggedForInspectionTypeAndCode(String inspectionType, String code) {
        System.out.println(String.format("validateTicketIsFlaggedForInspectionTypeAndCode %s , %s", inspectionType, code));
        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat("one ".equals("two"));
        softAssertions.assertThat(1 == 2);
        softAssertions.assertAll();
    }
}
