package steps;

import org.jbehave.core.annotations.When;

public class ActionSteps {

    @When("A normal fare ticket is used")
    public void useFareTicket() {
        System.out.println(" Using a fare ticket");
    }

    @When("The ticket is scanned during $period time")
    public void scanTicketOnPeriod(String period) {
        System.out.println(" Using a fare ticket for period: " + period);
    }

    @When("The ticket is scanned on the validator")
    public void scanTicketOnValidator() {
        System.out.println("Ticket is scanned on the validator ");
    }

    @When("The token is tapped on the validator")
    public void tapTokenOnValidator() {
        System.out.println("Token is tapped on the validator ");
    }
}
