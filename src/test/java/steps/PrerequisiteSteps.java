package steps;

import org.jbehave.core.annotations.Given;

public class PrerequisiteSteps {

    @Given("A customer is created")
    public void createCustomer() {
        System.out.println("Create customer");

    }


    @Given("Only the $period fare block")
    public void hasOnlyAPeakFareBlock(String period) {
        System.out.println(String.format("Create customer with %s fare block", period));
    }


    @Given("A validator has fareblock $type")
    public void setValidatorFareblock(String type) {
        System.out.println(String.format("A validator is generated with fareblock %s", type));
    }

    @Given("A validator has no active fareblocks")
    public void setAValidatorWithNoActiveFareblocks() {
        System.out.println("Setting a validator with no active fareblocks");
//        SoftAssert softAssert = new SoftAssert();
//        softAssert.assertEquals(1,2,"Expected message one equals 2");
//        softAssert.assertEquals("one ","two","Expected message one equals 2");
//        softAssert.assertEquals(1,1,"Expected message one equals 2");
//        softAssert.assertAll();
    }


    @Given("$isCorrect fareblocks are configured to result in $result")
    public void fareblocksConfiguredToReturn(String isCorrect, String result) {
        System.out.println(String.format("fareblocksConfiguredToReturn %s , %s ",isCorrect,result ));
    }

    @Given("A token has a healthy balance")
    public void setATokenWithHealthyBalance() {
        System.out.println("setNoProducts ");
    }

    @Given("Has a day pass")
    public void setADayPass() {
        System.out.println("setNoProducts ");
    }

    @Given("Has no products")
    public void setNoProducts() {
        System.out.println("setNoProducts ");
    }

    @Given("It is actually $period time")
    public void setActualPeriod(String period) {
        System.out.println(String.format("setActualPeriod %s ",period ));
    }

    @Given("A validator has no configured fareblocks")
    public void setValidatorWithConfigurationForFareblocks() {
        System.out.println("setValidatorWithConfigurationForFareblocks ");
    }

    @Given("A token has $funds funds")
    public void setupTokenWithFunds(String funds) {
        System.out.println(String.format("setupTokenWithFunds %s ",funds ));
    }

    @Given("Has no provisioned products")
    public void setupNoProvisionedProducts() {
        System.out.println("setupNoProvisionedProducts ");
    }

    @Given("Has been provisioned with a single use product that day.")
    public void setupProvisionedWithProduct() {
        System.out.println("setupProvisionedWithProduct ");
//        BrokerAdapter brokerAdapter = new BrokerAdapter();
//        ProductLookUpResponse productLookUpResponse = brokerAdapter.getAbtProductLookup(BrandID.BRAND_ID1,"","","");
    }

    @Given("An onboard validator is located on a vehicle that has just pulled into station $station")
    public void setOnboardValidatorHasPulledIntoStation(String station) {
        System.out.println(String.format("setOnboardValidatorHasPulledIntoStation %s ",station ));
    }

    @Given("A validator share a common fareblock")
    public void aValidatorShareFareblock() {
        System.out.println("aValidatorShareFareblock ");
    }


    @Given("Has set their phone time to the $period time")
    public void simulatePhoneToPeriodTime(String period) {
        System.out.println(String.format("simulatePhoneToPeriodTime %s ",period ));
    }


    @Given("The price difference between the single use product and a day pass is less than the cost of another single use product")
    public void setPriceDifferenceForDifferentTypesOfProduct() {
        System.out.println("setPriceDifferenceForDifferentTypesOfProduct ");
    }
}
