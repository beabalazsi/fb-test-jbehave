package steps;

import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;

public class TicketPrerequisiteSteps {

    @Given("A rider has a single ticket with $entitlementType entitlements")
    @Alias("A rider has a ticket with $type entitlement")
    public void singleTicketCustomerWithNoEntitlement(String entitlementType){
        System.out.println(String.format("Create a customer of %s entitlement",entitlementType));
    }

    @Given("A ticket has fareblock $type")
    public void setTicketFareblock(String type){
        System.out.println(String.format("A ticket is generated with fareblock %s",type));
    }

    @Given("A rider has a ticket that is $isValid valid at station $station")
    public void aRiderHasTicketInStation(String isValid, String station){
        System.out.println(String.format("aRiderHasTicketInStation %s , %s",isValid,station ));
    }

    @Given("A ticket $type")
    public void createTicketOfType(String type){
        System.out.println(String.format("createTicketOfType %s ",type ));
    }

    @Given("A rider has an $period ticket")
    public void createRiderWithPeriodTicket(String period){
        System.out.println(String.format("createRiderWithPeriodTicket %s ",period ));
    }
}
