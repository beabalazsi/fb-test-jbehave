package steps;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeScenario;

public class Hooks {

    @BeforeScenario
    public void startSystem() throws Exception {
        System.out.println("I am before scenario");
    }

    @AfterScenario
    public void stopSystem() throws Exception {
        System.out.println("I am after scenario");
    }

}
