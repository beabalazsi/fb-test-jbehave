package model;

public class ProvisionedFare {
    private String id;
    private int maxTrips;
    private int noTrips;
    private String earliestEvent;
    private String latestEvent;
    private String lastUpdated;
    private Product product;
    private Cost cost;
}
