package model;

import java.util.Set;

public class ExternalTrip {
    private String id;
    // <date-time>
    private String start;
    private Set<String> fareBlocks;
    private TripStatus status;
}
