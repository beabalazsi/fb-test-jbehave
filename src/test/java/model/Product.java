package model;

import java.util.Set;

public class Product {
    private String ref;
    private String name;
    private String fareType;
    private String startTime;
    private String endTime;
    private String validitySpecification;
    private Set<Integer> fareBlocks;
}
