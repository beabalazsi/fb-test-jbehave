package model;

import java.util.Set;

public class ConsideredProduct {
    private String name;
    private int cost;
    private String ref;
    private int maxTrips;
    private String validitySpecification;
    private Set<Integer> fareBlocks;
}
