package model;

import java.util.List;

public class FareEngineCalculationResponse {
    private String result;
    private List<ConsideredProduct> productsConsidered;
    private List<AppliedRule> rulesApplied;
    private List<FareEngineOutcome> rejectedOutcomes;
    private FareUpdate fareUpdate;

}
